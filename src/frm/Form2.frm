VERSION 5.00
Begin VB.Form Form2 
   Caption         =   "Form2"
   ClientHeight    =   3105
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   5640
   LinkTopic       =   "Form2"
   ScaleHeight     =   3105
   ScaleWidth      =   5640
   StartUpPosition =   3  'Windows の既定値
   Begin VB.TextBox Text1 
      Height          =   1935
      Left            =   0
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  '両方
      TabIndex        =   0
      Text            =   "Form2.frx":0000
      Top             =   0
      Width           =   2775
   End
   Begin VB.Menu mnuClose 
      Caption         =   "閉じる"
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub SetText(strTitle As String, strText As String)
    Me.Caption = strTitle
    Text1.Text = strText
End Sub

Private Sub Form_Resize()
    If Me.WindowState = vbMinimized Then Exit Sub
    
    Text1.Width = Me.ScaleWidth
    Text1.Height = Me.ScaleHeight
    
End Sub

Private Sub mnuClose_Click()
    Unload Me
End Sub
